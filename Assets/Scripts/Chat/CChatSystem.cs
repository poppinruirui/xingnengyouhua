﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ExitGames.Client.Photon;
public class CChatSystem : MonoBehaviour {// , IChatClientListener {

    public static CChatSystem s_Instance;

	public InputField _inputContent;
	public bool m_bShowing = false;

    public GameObject _preChatMessage;
    public GameObject _goMessageContainer;

    public int m_nFontSize = 18;
    public float m_fWidthOfOneLine = 400f;
    public float m_fHeightOfOneLine = 30f;

    public Font m_Font;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public string m_szSenderColor = "00FF00";

    public float m_fMaskBorderY = 300f;

    void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
        
      //this.chatClient = new ChatClient(this);//  
      // this.chatClient.Connect(PhotonNetwork.PhotonServerSettings.ChatAppID, "1", new ExitGames.Client.Photon.Chat.AuthenticationValues( MapEditor.GetMainPlayerName() ));//  
    }

    // Update is called once per frame
    void Update () {

       // if (this.chatClient != null)
        //    this.chatClient.Service();

        if (Input.GetKeyDown (KeyCode.Return)) {
			if (!m_bShowing) {
				this.SetVisile ( true );
				m_bShowing = true;
			} else {
				this.SetVisile ( false );
				m_bShowing = false;
				CGMSystem.s_Instance.ProcessInstruction ( _inputContent.text );

                // this.chatClient.PublishMessage("world", _inputContent.text);   
                Main.s_Instance.m_MainPlayer.PublishMessage(_inputContent.text);
            }
		}
	}

    public void ReceiveMessage( string szSender, string szMessage )
    {
        float fHeightOfNewMessage = AddOneMessage(szSender, szMessage);
        MoveOldMessage(fHeightOfNewMessage);
    }

    void MoveOldMessage ( float fHeightOfNewMessage )
    {
        for ( int i = m_lstMessages.Count - 2; i >= 0; i-- )
        {
            CChatMessage message = m_lstMessages[i];
            vecTempPos = message.transform.localPosition;
            vecTempPos.y += fHeightOfNewMessage;
            message.transform.localPosition = vecTempPos;
            if (message.transform.localPosition.y > m_fMaskBorderY + message.GetGeight())
            {
                RecycleMessage(message);
                m_lstMessages.RemoveAt(i);
                continue;
            }
        }
    }


    public void SetVisile( bool val )
	{
		_inputContent.gameObject.SetActive ( val );
		if (val) {
			_inputContent.ActivateInputField ();
		}
	}

	public bool IsShowing()
	{
        return m_bShowing;
	}

    public bool IsChatBarFocus()
    {
        return _inputContent.isActiveAndEnabled;
    }

    float GetTextWidth( string szText )
    {
        m_Font.RequestCharactersInTexture(szText, m_nFontSize, FontStyle.Normal);
        float width = 0f;
        CharacterInfo characterInfo;
        for (int i = 0; i < szText.Length; i++)
        {
            m_Font.GetCharacterInfo(szText[i], out characterInfo, m_nFontSize);
            width += characterInfo.advance;
        }

        return width;
    }

    float AddOneMessage(string szSender, string szMessage)
    {
        string szContent = szSender +  "：" + szMessage;
        float fContentWidth = GetTextWidth(szContent);
        szContent = "<color=#" + m_szSenderColor + ">" + szSender + "</color>" + "：" + szMessage;
        float fNumOfLine = fContentWidth / m_fWidthOfOneLine;
        int nNumOfLine = (int)Mathf.Ceil(fNumOfLine);
        float fHeightOfThisMessage = nNumOfLine * m_fHeightOfOneLine;
        vecTempPos.x = 0f;
        vecTempPos.y = fHeightOfThisMessage;
        vecTempPos.z = 0f;
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        CChatMessage goMessgae = ReuseMessage();
        goMessgae.transform.localPosition = vecTempPos;
        goMessgae.transform.localScale = vecTempScale;
        goMessgae.SetSize(m_fWidthOfOneLine, fHeightOfThisMessage);
        goMessgae.SetContent(szContent);
        m_lstMessages.Add(goMessgae);
        return fHeightOfThisMessage;
    }

    CChatMessage ReuseMessage()
    {
        CChatMessage goMessage = null;
        if (m_lstRecycledMessages.Count > 0)
        {
            goMessage = m_lstRecycledMessages[0];
            m_lstRecycledMessages.RemoveAt(0);
        }
        else
        {
            goMessage = GameObject.Instantiate(_preChatMessage).GetComponent<CChatMessage>();
            goMessage.transform.SetParent(_goMessageContainer.transform);
        }

        return goMessage;
    }

    List<CChatMessage> m_lstMessages = new List<CChatMessage>();
    List<CChatMessage> m_lstRecycledMessages = new List<CChatMessage>();
    void RecycleMessage(CChatMessage goMessage)
    {
        m_lstRecycledMessages.Add(goMessage);
    }


    /*
    #region  
    public void DebugReturn(DebugLevel level, string message)
    { }

    public void OnDisconnected()
    {
        Debug.Log("**************************   DisConnected");
    }
    public void OnConnected()
    {
		Debug.Log ( "Chat system connected!" );
    }

    public void OnChatStateChange(ChatState state)
    { }
    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        for (int i = 0; i < senders.Length; i++)
        {
            AddOneMessage( senders[i], messages[i].ToString() );
        }
        
    }

 

    public void OnPrivateMessage(string sender, object message, string channelName)
    { }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        
        //connectionState.text = "In World Chat";
        //this.chatClient.PublishMessage(channels, "Joined");
    }

    public void OnUnsubscribed(string[] channels)
    { }  

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    { }
    #endregion

    */
}
