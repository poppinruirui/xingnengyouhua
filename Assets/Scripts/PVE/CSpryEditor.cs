﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CSpryEditor : MonoBehaviour {

	static Vector3 vecTempPos = new Vector3();
	static Vector3 vecTempScale = new Vector3();

	public static  CSpryEditor s_Instance; 

	public GameObject m_goSprayContainer;

	public Toggle _toogleShowRealtimeBeanSpray;
	public Dropdown _dropdownSprayId;
	public InputField _inputfieldThornId;
	public InputField _inputfieldDensity;
	public InputField _inputfieldMeatDensity;
	public InputField _inputfieldMaxDis;
	public InputField _inputfieldMinDis;
	public InputField _inputfieldBeanLifeTime;
	public InputField _inputfieldSprayInterval;
	public InputField _inputfieldSpraySize;

	public struct sSprayConfig
	{
		public int nConfigId;
		public string  szThornId;
		public float fDensity;
		public float fMaxDis;
		public float fMinDis;
		public float fLifeTime;
		public float fInterval;
		public float fSpraySize;
	};
	Dictionary<int, sSprayConfig> m_dicSprayConfig = new Dictionary<int, sSprayConfig>();
	sSprayConfig m_CurSprayConfig;


	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		InitDropdown_SprayId ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitDropdown_SprayId()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "0");
		showNames.Add( "1");
		showNames.Add( "2");
		showNames.Add( "3");
		showNames.Add( "4");
		showNames.Add( "5");
		showNames.Add( "6");
		showNames.Add( "7");
		showNames.Add( "8");
		UIManager.UpdateDropdownView(_dropdownSprayId, showNames);
	}

	sSprayConfig tempSprayConfig;
	public sSprayConfig GetSprayConfigById( int nId )
	{
		if (!m_dicSprayConfig.TryGetValue (nId, out tempSprayConfig)) {
			tempSprayConfig = new sSprayConfig ();
			tempSprayConfig.nConfigId = nId;
			m_dicSprayConfig [nId] = tempSprayConfig;
		}
		return tempSprayConfig;
	}

	public void OnButtonClick_AddSpray()
	{
		CSpray spray = ResourceManager.s_Instance.ReuseSpray ();
		spray.transform.parent = m_goSprayContainer.transform;
		vecTempPos.x = 50;
		vecTempPos.y = 50;
		vecTempPos.z = -300;
		spray.SetPos ( vecTempPos );
        float fScale = CyberTreeMath.Radius2Scale(m_CurSprayConfig.fSpraySize);
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		spray.SetScale ( vecTempScale );

	}

	void UpdateUiContent ()
	{
		_inputfieldThornId.text = m_CurSprayConfig.szThornId;
		_inputfieldMaxDis.text = m_CurSprayConfig.fMaxDis.ToString();
		_inputfieldMinDis.text = m_CurSprayConfig.fMinDis.ToString ();
		_inputfieldDensity.text = m_CurSprayConfig.fDensity.ToString ();
		_inputfieldSprayInterval.text = m_CurSprayConfig.fInterval.ToString ();
		_inputfieldBeanLifeTime.text = m_CurSprayConfig.fLifeTime.ToString ();
		_inputfieldSpraySize.text = m_CurSprayConfig.fSpraySize.ToString ();
	}

	public void OnDropDownValueChanged_SprayId()
	{
		m_CurSprayConfig = GetSprayConfigById ( _dropdownSprayId.value );
		UpdateUiContent ();
	}

	public void OnInputValueChanged_ThornId()
	{
		m_CurSprayConfig.szThornId = _inputfieldThornId.text;
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}

	public void OnInputValueChanged_MaxDis()
	{
		m_CurSprayConfig.fMaxDis = float.Parse ( _inputfieldMaxDis.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}
	public void OnInputValueChanged_MinDis()
	{
		m_CurSprayConfig.fMinDis = float.Parse ( _inputfieldMinDis.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}

	public void OnInputValueChanged_LifeTime()
	{
		m_CurSprayConfig.fLifeTime = float.Parse ( _inputfieldBeanLifeTime.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}

	public void OnInputValueChanged_Interval()
	{
		m_CurSprayConfig.fInterval = float.Parse ( _inputfieldSprayInterval.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}

	public void OnInputValueChanged_Density()
	{
		m_CurSprayConfig.fDensity = float.Parse ( _inputfieldDensity.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;
	}

	public void OnInputValueChanged_SpraySize()
	{
		m_CurSprayConfig.fSpraySize = float.Parse ( _inputfieldSpraySize.text );
		m_dicSprayConfig [m_CurSprayConfig.nConfigId] = m_CurSprayConfig;


		CSpray spray = CPveEditor.s_Instance.GetCurSelectedObj () as CSpray;
		if (spray != null) {
            float fScale = CyberTreeMath.Radius2Scale(m_CurSprayConfig.fSpraySize);
			vecTempScale.x = fScale;
			vecTempScale.y = fScale;
			vecTempScale.z = 1f;
			spray.SetScale (vecTempScale);
		}
	}

	void SaveSprayInstance ( XmlDocument xmlDoc, XmlNode node )
	{
		int nGuid = 0;
		foreach (Transform child in m_goSprayContainer.transform) {
			CSpray spray = child.gameObject.GetComponent<CSpray> ();
			XmlNode spray_instance_node = StringManager.CreateNode (xmlDoc, node, "G" + nGuid ); 
			nGuid++;
			StringManager.CreateNode (xmlDoc, spray_instance_node, "ConfigId", spray.GetConfigId().ToString() );
			StringManager.CreateNode (xmlDoc, spray_instance_node, "PosX", spray.GetPos().x.ToString( "f1" ) );
			StringManager.CreateNode (xmlDoc, spray_instance_node, "PosY", spray.GetPos().y.ToString( "f1" ) );	
			StringManager.CreateNode (xmlDoc, spray_instance_node, "Size", spray.GetScale().x.ToString( "f1" ) );	
		} // end foreach
	}

	public void SaveSpray(XmlDocument xmlDoc, XmlNode node)
	{
		foreach (KeyValuePair<int, sSprayConfig> pair in m_dicSprayConfig ) {
			if (pair.Value.fDensity == 0f) {
				continue;
			}
			tempSprayConfig = pair.Value;
			XmlNode spray_config_node = StringManager.CreateNode (xmlDoc, node, "S" + pair.Key ); 
			StringManager.CreateNode (xmlDoc, spray_config_node, "ConfigId", pair.Value.nConfigId.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "ThornId", pair.Value.szThornId );
			StringManager.CreateNode (xmlDoc, spray_config_node, "Density", pair.Value.fDensity.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "MaxDis", pair.Value.fMaxDis.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "MinDis", pair.Value.fMinDis.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "LifeTime", pair.Value.fLifeTime.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "Interval", pair.Value.fInterval.ToString());
			StringManager.CreateNode (xmlDoc, spray_config_node, "SpraySize", pair.Value.fSpraySize.ToString());

		} // end foreach

		XmlNode instance_node = StringManager.CreateNode (xmlDoc, node, "Instance" ); 
		SaveSprayInstance ( xmlDoc, instance_node );


	}

	public void GenerateSprayInstance( XmlNode node )
	{
		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode ins_node = node.ChildNodes [i];
			CSpray spray = ResourceManager.s_Instance.ReuseSpray();
			spray.transform.parent = m_goSprayContainer.transform;
			vecTempPos.z = 0f;
			vecTempScale.z = 1f;
			for (int j = 0; j < ins_node.ChildNodes.Count; j++) {
				XmlNode param_node = ins_node.ChildNodes[j];
				switch(param_node.Name)
				{
				case "ConfigId":
					{
						int nConfigId = int.Parse ( param_node.InnerText );
						spray.SetConfigId ( nConfigId );
						sSprayConfig sprayConfig = GetSprayConfigById ( nConfigId );
						spray.SetConfig ( sprayConfig );

					}
					break;
				case "PosX":
					{
						vecTempPos.x = float.Parse ( param_node.InnerText );
					}
					break;
				case "PosY":
					{
						vecTempPos.y = float.Parse ( param_node.InnerText );
					}
					break;		
				case "Size":
					{
						vecTempScale.x = vecTempScale.y = CyberTreeMath.Radius2Scale( float.Parse ( param_node.InnerText ) );

					}
					break;
				} // end switch
			} // end for j
			spray.SetScale ( vecTempScale );
            spray.SetPos(vecTempPos);
        } // end for i
	}

	public void GenerateSpray( XmlNode node )
	{
		if (node == null || node.ChildNodes.Count == 0) {
			return;
		}

		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode spray_config_node = node.ChildNodes [i];

			if (spray_config_node.Name == "Instance") {
				GenerateSprayInstance ( spray_config_node );
				continue;
			}

			int nSprayId = int.Parse (spray_config_node.Name.Substring (1, spray_config_node.Name.Length - 1));
			tempSprayConfig = GetSprayConfigById (nSprayId);
			tempSprayConfig.nConfigId = nSprayId;
			for (int j = 0; j < spray_config_node.ChildNodes.Count; j++) {
				XmlNode node_param = spray_config_node.ChildNodes [j];
				switch (node_param.Name) {
				case "ThornId":
					{
						tempSprayConfig.szThornId = node_param.InnerText;
					}
					break;

				case "Density":
					{
						tempSprayConfig.fDensity = float.Parse (node_param.InnerText);
					}
					break;
				case "MaxDis":
					{
						tempSprayConfig.fMaxDis = float.Parse (node_param.InnerText);
					}
					break;
				case "MinDis":
					{
						tempSprayConfig.fMinDis = float.Parse (node_param.InnerText);
					}
					break;
				case "LifeTime":
					{
						tempSprayConfig.fLifeTime = float.Parse (node_param.InnerText);
					}
					break;
				case "Interval":
					{
						tempSprayConfig.fInterval = float.Parse (node_param.InnerText);
					}
					break;
				case "SpraySize":
					{
						tempSprayConfig.fSpraySize = float.Parse (node_param.InnerText);
					}
					break;
				}// end switch
			}// end j
			m_dicSprayConfig [tempSprayConfig.nConfigId] = tempSprayConfig;
		} //  end i
		m_CurSprayConfig = GetSprayConfigById (0);
		UpdateUiContent ();
	}
}
