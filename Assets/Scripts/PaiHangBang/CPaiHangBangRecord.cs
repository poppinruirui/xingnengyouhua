﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPaiHangBangRecord : MonoBehaviour {

    public Image _imgAvatar;
    public Text _txtPlayerName;
    public Text _txtPlayerLevel;
    public Text _txtJiShaInfo;
    public Text _txtTotalVolume;

    public Text[] _aryTxtItemLevel;

    public Image _imgSelectedSkillIcon;
    public Text _txtSelectedSkillLevel;

    public Text _txtEatThornNum;

    public Image _imgHighLight;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetInfo( CPaiHangBang_Mobile.sPlayerAccount info )
    {
        //_imgAvatar.sprite = ResourceManager.s_Instance.GetBallSpriteByPlayerId( info.nOwnerId );
        _imgAvatar.sprite = ResourceManager.s_Instance.GetSkinSprite(info.nOwnerId);

        if (info.bDead)
        {
            _imgAvatar.material = ResourceManager.s_Instance.m_matGray;
        }
        else
        {
            _imgAvatar.material = null;
        }

        _txtPlayerName.text = info.szName;
        _txtPlayerLevel.text = info.nLevel.ToString();
        _txtTotalVolume.text = CyberTreeMath.K_Style( info.fTotalVolume );
        _txtJiShaInfo.text = info.nKillNum + " / " + info.nBeKilledNum + " / " + info.nAssistNum;

        _txtSelectedSkillLevel.text = info.nSelectedSkillLevel.ToString();
        _imgSelectedSkillIcon.sprite = CSkillSystem.s_Instance.GetSkillSprite( info.nSelectedSkillId );

        _aryTxtItemLevel[1].text = info.nItem1Num.ToString();
        _aryTxtItemLevel[2].text = info.nItem2Num.ToString();
        _aryTxtItemLevel[3].text = info.nItem3Num.ToString();

        _txtEatThornNum.text = info.nEatThornNum.ToString();

        if (info.bIsMainPlayer)
        {
            _imgHighLight.gameObject.SetActive(true);
        }
        else
        {
            _imgHighLight.gameObject.SetActive(false);
        }
    }
}
