﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Net;
using UnityEngine.Networking;
using System.Xml;
public class CRegister : MonoBehaviour
{
    const string ip = "http://118.24.109.32:9000";
    const string url_getvericcode = ip + "/smscode";
    const string url_register = ip + "/register";
    const string url_loginbypassword = ip + "/passwordlogin";
    const string url_autologin = ip + "/sessionlogin";
    const string url_createrole = ip + "/updaterolename";

    public InputField _inputPhoneNum;
    public InputField _inputVeriCode;
    public InputField _inputPassword;

    public InputField _inputPhoneNumWhenLogin;
    public InputField _inputPasswordWhenLogin;

    public Toggle _toggleAutoLogin;

    public InputField _inputRoleName;

    public struct sAccountInfo
    {
        public string szPhoneNum;
        public string szSessionId;
    };

    Dictionary<string, sAccountInfo> m_dicAccountInfo = new Dictionary<string, sAccountInfo>();
    sAccountInfo m_CurAccoutInfo;
    string m_szCurLoginPhoneNum = "";

    // Use this for initialization
    void Start()
    {
        LoadAccountInfo();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClickBtn_GetVeriCode()
    {
        StartCoroutine(GetVeriCode());
    }


    IEnumerator GetVeriCode()
    {
        string request = url_getvericcode + "?phonenum=" + _inputPhoneNum.text + "&smstype=" + "1";
        UnityWebRequest www = UnityWebRequest.Get(request);



        yield return www.Send();
        Debug.Log(www.downloadHandler.text);
    }

    public void OnClickBtn_Register()
    {
        StartCoroutine(DoRegister());
    }


    IEnumerator DoRegister()
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", _inputPhoneNum.text);
        form.AddField("smscode", _inputVeriCode.text);
        form.AddField("password", _inputPassword.text);
        UnityWebRequest www = UnityWebRequest.Post(url_register, form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
    }

    public void OnClickBtn_Login()
    {
        m_szCurLoginPhoneNum = _inputPhoneNumWhenLogin.text;

        if (_toggleAutoLogin.isOn) // 自动登录
        {
            if (!m_dicAccountInfo.TryGetValue(m_szCurLoginPhoneNum, out m_CurAccoutInfo))
            {
                Debug.Log("该手机号尚未在本机成功登录过，不能自动登录");
                return;
            }
            else
            {
                StartCoroutine(DoLoginAuto());
            }
        }
        else // 账号密码登录
        {
            StartCoroutine(DoLoginByPassword());
        }
    }

    IEnumerator DoLoginByPassword()
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", _inputPhoneNumWhenLogin.text);
        form.AddField("password", _inputPasswordWhenLogin.text);
        UnityWebRequest www = UnityWebRequest.Post(url_loginbypassword, form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);

        string[] aryTemp1 = www.downloadHandler.text.Split(',');
        string[] aryTemp2 = aryTemp1[0].Split(':');

        if (aryTemp2[1] == "0") // 成功
        {
            string[] aryTemp3 = aryTemp1[1].Split(':');


            sAccountInfo account_info = new sAccountInfo();
            account_info.szPhoneNum = m_szCurLoginPhoneNum;
            account_info.szSessionId = aryTemp3[1];
            account_info.szSessionId = account_info.szSessionId.Substring(0, account_info.szSessionId.Length - 1);
            m_dicAccountInfo[account_info.szPhoneNum] = account_info;

            SaveAccountInfo();
        }
    }

    IEnumerator DoLoginAuto()
    {
        string request = url_autologin + "?phonenum=" + m_szCurLoginPhoneNum + "&sessionid=" + m_CurAccoutInfo.szSessionId; //"1794142616"
        UnityWebRequest www = UnityWebRequest.Get(request);
        yield return www.Send();
        Debug.Log(www.downloadHandler.text);
    }

    void SaveAccountInfo()
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
        xmlDoc.AppendChild(node);
        XmlNode root = xmlDoc.CreateElement("root");
        xmlDoc.AppendChild(root);
        foreach (KeyValuePair<string, sAccountInfo> pair in m_dicAccountInfo)
        {
            XmlNode sub_node = xmlDoc.CreateNode(XmlNodeType.Element, "P" + pair.Key, null);
            sub_node.InnerText = pair.Value.szSessionId;
            root.AppendChild(sub_node);
        }
        xmlDoc.Save(Application.dataPath + "/StreamingAssets/LoginSession.xml");
    }

    void LoadAccountInfo()
    {
        string szFileName = Application.streamingAssetsPath + "/LoginSession.xml";

        if (!System.IO.File.Exists(szFileName))
        {
            Debug.Log("文件不存在");
            return;
        }

        XmlDocument myXmlDoc = new XmlDocument();
        myXmlDoc.Load(szFileName);
        XmlNode root = null;
        root = myXmlDoc.SelectSingleNode("root");
        if (root == null)
        {
            return;
        }

        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            XmlNode node = root.ChildNodes[i];
            sAccountInfo account_info = new sAccountInfo();
            account_info.szPhoneNum = node.Name.Substring(1, node.Name.Length - 1);
            account_info.szSessionId = node.InnerText;
            m_dicAccountInfo[account_info.szPhoneNum] = account_info;
        }
    }

    public void OnClickBtn_CreateRole()
    {
        StartCoroutine(DoCreateRole());
    }


    IEnumerator DoCreateRole()
    {
        WWWForm form = new WWWForm();
        form.AddField("phonenum", m_CurAccoutInfo.szPhoneNum);
        form.AddField("sessionid", m_CurAccoutInfo.szSessionId);
        form.AddField("rolename", _inputRoleName.text);
        UnityWebRequest www = UnityWebRequest.Post(url_createrole, form);
        yield return www.SendWebRequest();
        Debug.Log(www.downloadHandler.text);
    }
}