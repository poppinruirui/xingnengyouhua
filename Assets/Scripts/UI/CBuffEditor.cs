﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CBuffEditor : MonoBehaviour {

	public static CBuffEditor s_Instance;

	public InputField[] _aryDesc;
	public InputField[] _aryValues;
	public InputField _inputTime;
	public Dropdown _dropdownBuffId;
	public Dropdown _dropdownBuffFunc;

	public enum eBuffFunc
	{
		player_move_speed,            // 玩家移动速度
		player_spit_spore_speed,      // 吐孢子速度
		player_spit_spore_distance,   // 吐孢子距离
		explode_child_num,            // 影响炸球的分球数
		explode_mother_left_percent,  // 影响炸球时母球的保留体积
		shell_time,                   // 影响壳的持续时间

	}

	const int c_FloatValueNum = 3;
	public struct sBuffConfig
	{
		public int id;
		public int func;
		public float time;
		public string[] aryDesc;
		public float[] aryValues;
	};
	Dictionary<int, sBuffConfig> m_dicBuffConfig = new Dictionary<int, sBuffConfig>();
	sBuffConfig m_CurEditConfig;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		m_CurEditConfig = GetBuffConfigById (0);
		InitDropDown_BuffId ();
		InitDropDown_BuffFunc ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void InitDropDown_BuffId()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "没有0号Buff哈");
		showNames.Add( "1号Buff");
		showNames.Add( "2号Buff");
		showNames.Add( "3号Buff");
		showNames.Add( "4号Buff");
		showNames.Add( "5号Buff");
		showNames.Add( "6号Buff");
		showNames.Add( "7号Buff");
		showNames.Add( "8号Buff");
		showNames.Add( "9号Buff");
		showNames.Add( "10号Buff");
		showNames.Add( "11号Buff");
		showNames.Add( "12号Buff");
		showNames.Add( "13号Buff");
		showNames.Add( "14号Buff");
		showNames.Add( "15号Buff");
		showNames.Add( "16号Buff");
		showNames.Add( "17号Buff");
		showNames.Add( "18号Buff");


		UIManager.UpdateDropdownView( _dropdownBuffId, showNames);
	}

	void InitDropDown_BuffFunc()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "影响球球移动速度");
		showNames.Add( "影响吐孢子的射速");
		showNames.Add( "影响吐孢子的距离");
		showNames.Add( "影响炸球的分球数");
		showNames.Add( "影响炸球时母球的保留体积");
		showNames.Add( "影响壳的持续时间");
		//showNames.Add( "隐蔽草丛");
		//showNames.Add( "速度加成草丛");
		//showNames.Add( "清除壳草丛");

		UIManager.UpdateDropdownView( _dropdownBuffFunc, showNames);
	}

	public void OnDropDownValueChanged_BuffId()
	{
		m_CurEditConfig = GetBuffConfigById ( _dropdownBuffId.value );
		UpdateUiContent ();
	}

	public void OnDropDownValueChanged_BuffFunc()
	{
		m_CurEditConfig.func = _dropdownBuffFunc.value;
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	void UpdateUiContent (  )
	{
		for (int i = 0; i < c_FloatValueNum; i++) {
			_aryValues [i].text = m_CurEditConfig.aryValues [i].ToString ();
			_aryDesc [i].text = m_CurEditConfig.aryDesc [i];
		}
		_dropdownBuffFunc.value = m_CurEditConfig.func;
		_inputTime.text = m_CurEditConfig.time.ToString ();
	}

	sBuffConfig tempConfig;
	public sBuffConfig GetBuffConfigById( int nId )
	{
		if (!m_dicBuffConfig.TryGetValue ( nId, out tempConfig )) {
			tempConfig = new sBuffConfig ();
			tempConfig.aryDesc = new string[c_FloatValueNum];
			tempConfig.id = nId;
			tempConfig.aryValues = new float[c_FloatValueNum];
			m_dicBuffConfig [nId] = tempConfig;
		}
		return tempConfig;
	}

	public void OnInputValueChanged_Time()
	{
		m_CurEditConfig.time = float.Parse ( _inputTime.text );
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_Desc0()
	{
		m_CurEditConfig.aryDesc[0] = _aryDesc[0].text;
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_Desc1()
	{
		m_CurEditConfig.aryDesc[1] = _aryDesc[1].text;
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_Desc2()
	{
		m_CurEditConfig.aryDesc[2] = _aryDesc[2].text;
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_FloatValue0()
	{
		m_CurEditConfig.aryValues [0] = float.Parse( _aryValues [0].text );
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_FloatValue1()
	{
		m_CurEditConfig.aryValues [1] = float.Parse( _aryValues [1].text );
		m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void OnInputValueChanged_FloatValue2()
	{
		m_CurEditConfig.aryValues [2] = float.Parse( _aryValues [2].text );
     	m_dicBuffConfig [m_CurEditConfig.id] = m_CurEditConfig;
	}

	public void SaveBuff( XmlDocument xmlDoc, XmlNode node  )
	{
		foreach (KeyValuePair<int, sBuffConfig> pair in m_dicBuffConfig) {
			XmlNode buff_node = StringManager.CreateNode (xmlDoc, node, "Buff" + pair.Key ); 
			StringManager.CreateNode (xmlDoc, buff_node, "func", pair.Value.func.ToString());
			StringManager.CreateNode (xmlDoc, buff_node, "time", pair.Value.time.ToString());
			for (int i = 0; i < c_FloatValueNum; i++) {
				StringManager.CreateNode (xmlDoc, buff_node, "desc" + i, pair.Value.aryDesc [i]);
				StringManager.CreateNode (xmlDoc, buff_node, "value" + i, pair.Value.aryValues [i].ToString ());
			} // end for i
		} // end foreach
	}

	public void GenerateBuff( XmlNode nodeBuff )
	{
		if (nodeBuff == null) {
			return;
		}
		
		for (int i = 0; i < nodeBuff.ChildNodes.Count; i++) {
			XmlNode node_buff = nodeBuff.ChildNodes [i];
			int nBuffId = int.Parse( node_buff.Name.Substring ( 4, node_buff.Name.Length - 4 ) );
			sBuffConfig config = GetBuffConfigById ( nBuffId );
			for (int j = 0; j < node_buff.ChildNodes.Count; j++) {
				XmlNode node_info = node_buff.ChildNodes [j];
				if (node_info.Name.Contains( "desc" ) ) {
					int nIndex = int.Parse (node_info.Name.Substring (4, node_info.Name.Length - 4));
					config.aryDesc[nIndex] = node_info.InnerText;
				}
				if (node_info.Name.Contains( "value" ) ) {
					int nIndex = int.Parse (node_info.Name.Substring (5, node_info.Name.Length - 5));
					config.aryValues[nIndex] = float.Parse (node_info.InnerText);
				}
				if (node_info.Name == "func") {
					config.func = int.Parse ( node_info.InnerText );
				}
				if (node_info.Name == "time") {
					config.time = float.Parse ( node_info.InnerText );
				}
			} // end for j
			m_dicBuffConfig[config.id] = config;
		} // end for i

		UpdateUiContent ();
	}

	public bool CheckIfBuffIdValid( int nBuffId )
	{
		if (nBuffId == 0) {
			return false;
		}

		if (!m_dicBuffConfig.TryGetValue (nBuffId, out tempConfig)) {
			return false;
		}

		return true;
	}
}
