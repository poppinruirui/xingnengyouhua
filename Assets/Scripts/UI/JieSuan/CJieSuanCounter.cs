﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
public class CJieSuanCounter : MonoBehaviour {

    public GameObject _goZuiJiaKuang;

    public SkeletonGraphic _skeleLike;
    public Image _imgLiked_Click;
    public Image _imgLiked_NotClick;

    public CFrameAnimationEffect _effectAvatar;
    public CFrameAnimationEffect _effectOuterBorder;

    public CFrameAnimationEffect _effectQuanChangZuiJia_QianZou;
    public CFrameAnimationEffect _effectQuanChangZuiJia_ChiXu;   

    public Image _imgAvatar;
    public Text _txtName;
    public Text _txtVolume;
    public Text _CommonInfo;
    public Text _EatVolume;
    public Text _txtLikeNum;

    int m_nZuiJiaKuangStatus = 0;
    int m_nLikeBtnStatus = 0;
    float m_fLikeStatusTimeLapse = 0;

    static Vector3 vecTempScale = new Vector3();

    int m_nPlayerId = 0;
    public int m_nCounterId = 0;

    int m_nLikeNum = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ZuiJiaKuangLoop();
        LikeBtnLoop();
    }

    public void UpdateInfo(CPaiHangBang_Mobile.sPlayerAccount playerinfo )
    {
        m_nPlayerId = playerinfo.nOwnerId;

        _txtName.text = playerinfo.szName;
        _txtVolume.text = playerinfo.fTotalVolume.ToString( "f0" );
        _CommonInfo.text = playerinfo.nEatThornNum + "    " + playerinfo.nKillNum + " / " + playerinfo.nBeKilledNum + " / " + playerinfo.nAssistNum;

        int nSpriteIndex = playerinfo.nOwnerId % Main.s_Instance.m_aryBallSprite.Length;
        _imgAvatar.sprite = ResourceManager.s_Instance.GetBallSpriteByPlayerId (playerinfo.nOwnerId);
    }

    public void UpdateInfo( int nType )
    {
        if (nType == 0)
        {
            _txtName.text = "（无人实现击杀）";
        }
        else if (nType == 1)
        {
            _txtName.text = "（无人实现吃刺）";
        }
        _txtVolume.gameObject.SetActive( false );
        _CommonInfo.gameObject.SetActive(false);

    }

    public void ShowZuiJiaKuang()
    {
        m_nZuiJiaKuangStatus = 1;
        _goZuiJiaKuang.SetActive( true );
    }

    void ZuiJiaKuangLoop()
    {
        if (m_nZuiJiaKuangStatus != 1 )
        {
            return;
        }

        vecTempScale = _goZuiJiaKuang.transform.localScale;
        float fSpeed = CJieSuanManager.s_Instance.m_fZuiJiaKuangScaleSpeed * Time.deltaTime;
        vecTempScale.x -= fSpeed;
        vecTempScale.y -= fSpeed;
        if (vecTempScale.x <= 0.8f && vecTempScale.y <= 0.8f)
        {
            vecTempScale.x = 0.8f;
            vecTempScale.y = 0.8f;
            _goZuiJiaKuang.transform.localScale = vecTempScale;
            m_nZuiJiaKuangStatus = 2;

            _effectAvatar.gameObject.SetActive ( true);
          //  _effectOuterBorder.gameObject.SetActive(true);
        }
        _goZuiJiaKuang.transform.localScale = vecTempScale;
    }

    public void OnButtonClick_Like()
    {
        if (m_nLikeBtnStatus >= 1)
        {
            return; // 已经赞过了
        }

        _skeleLike.AnimationState.SetAnimation( 0, "aniClick", false );
        m_nLikeBtnStatus = 1;
        Main.s_Instance.m_MainPlayer.Like(m_nCounterId);
        //CJieSuanManager.s_Instance.Like(m_nCounterId);
    }
    
    void LikeBtnLoop()
    {
        if (m_nLikeBtnStatus != 1)
        {
            return;
        }

        m_fLikeStatusTimeLapse += Time.deltaTime;
        if (m_fLikeStatusTimeLapse > 0.5f)
        {
            m_nLikeBtnStatus = 2;
            _skeleLike.gameObject.SetActive( false );
        }
    }

    public void AddOneLike()
    {
        m_nLikeNum++;
        _txtLikeNum.text = m_nLikeNum.ToString();
    }
}

