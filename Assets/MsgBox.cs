﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MsgBox : MonoBehaviour {

	public enum eMsgBoxType
	{
		e_Msgbox_Type_None,
		e_Msgbox_Type_OkOnly,
		e_Msgbox_Type_YesNo,
	};
	eMsgBoxType m_eType = eMsgBoxType.e_Msgbox_Type_None;

    public static MsgBox s_Instance = null;
	public Button _btnOk;
	public Button _btnYes;
	public Button _btnNo;   
    public Text _txtContent;

	public delegate void DelegateMethod_OnClickButton( int nParam );
	public DelegateMethod_OnClickButton delegateMethodOnClickButton;   //声明了一个Delegate对象

	int m_nParam = 0;

    void Awake()
    {
        s_Instance = this;
    }

	// Use this for initialization
	void Start () {
		_btnOk.onClick.AddListener ( OnClickOK );
		_btnYes.onClick.AddListener ( OnClickYes );
		_btnNo.onClick.AddListener ( OnClickNo );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowMsg( string szContent, eMsgBoxType eType, int nParam = 0 )
    {
        _txtContent.text = szContent;
        this.gameObject.SetActive( true );
		m_nParam = nParam;
		SetType ( eType );
    }

	public void OnClickOK()
	{
		this.gameObject.SetActive ( false );       

	}

	public void OnClickYes()
	{
		this.gameObject.SetActive ( false );
		if (delegateMethodOnClickButton != null) {
			delegateMethodOnClickButton ( m_nParam );
		}
	}

	public void OnClickNo()
	{
		this.gameObject.SetActive ( false );

	}

	public void SetType( eMsgBoxType eType )
	{
		m_eType = eType;

		switch (eType) {
		case eMsgBoxType.e_Msgbox_Type_None:
			{
				_btnOk.gameObject.SetActive ( false );
				_btnNo.gameObject.SetActive ( false );
				_btnYes.gameObject.SetActive ( false );
			}
			break;
		case eMsgBoxType.e_Msgbox_Type_OkOnly:
			{
				_btnOk.gameObject.SetActive ( true );
				_btnNo.gameObject.SetActive ( false );
				_btnYes.gameObject.SetActive ( false );
			}
			break;
		case eMsgBoxType.e_Msgbox_Type_YesNo:
			{
				_btnOk.gameObject.SetActive ( false );
				_btnNo.gameObject.SetActive ( true );
				_btnYes.gameObject.SetActive ( true );
			}
			break;
		}
	}
}
