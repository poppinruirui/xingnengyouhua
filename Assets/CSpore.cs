﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSpore : MonoBehaviour
{

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempSize = new Vector3();

    int m_nPlayerId;
    uint m_uSporeId;
    float m_fVolume;
    float m_fRadius;

    Vector3 m_vDir = new Vector3();
    Vector3 m_vSpeed = new Vector3();
    Vector3 m_vDestPos = new Vector3();

    bool m_bEjecting = false;

    Ball m_Ball = null;

    public CircleCollider2D _Collider;
    public SpriteRenderer _srMain;

    public GameObject m_goContainer;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        Ejecting();
    }

    public void ResetAll()
    {
        m_Ball = null;
        m_bEjecting = false;
        SetDead(false);
    }

    public void SetColor(string szColor)
    {

    }

    public void SetVolume(float fVolume)
    {
        m_fVolume = fVolume;
        float fSize = CyberTreeMath.Volume2Scale(m_fVolume);
        m_fRadius = CyberTreeMath.Scale2Radius(fSize);
        SetSize(fSize);
    }

    public float GetVolume()
    {
        return m_fVolume;
    }

    public float GetRadius()
    {
        return m_fRadius;
    }

    public void SetSize(float fSize)
    {
        vecTempSize.x = fSize;
        vecTempSize.y = fSize;
        vecTempSize.z = 1f;
        this.transform.localScale = vecTempSize;
    }

    public void BeginEject(Vector3 dest)
    {
        SetDest(dest);

        m_bEjecting = true;
    }

    public bool IsEjecting()
    {
        return m_bEjecting;
    }

    void Ejecting()
    {
        if (!m_bEjecting)
        {
            return;
        }


        bool bXFinished = false;
        bool bYFinished = false;
        if (m_vSpeed.x == 0 || (m_vSpeed.x > 0 && GetPos().x >= m_vDestPos.x) || (m_vSpeed.x < 0 && GetPos().x <= m_vDestPos.x))
        {
            bXFinished = true;
        }

        if (m_vSpeed.y == 0 || (m_vSpeed.y > 0 && GetPos().y >= m_vDestPos.y) || (m_vSpeed.y < 0 && GetPos().y <= m_vDestPos.y))
        {
            bYFinished = true;
        }

        if (bXFinished && bYFinished)
        {
            EndEject();
        }

        vecTempPos = GetPos();

        if (!bXFinished)
        {
            vecTempPos.x += m_vSpeed.x * Time.fixedDeltaTime;
        }

        if (!bYFinished)
        {
            vecTempPos.y += m_vSpeed.y * Time.fixedDeltaTime;
        }

        SetPos(vecTempPos);
    }

    public void EndEject()
    {
        m_bEjecting = false;
    }

    public void SetSporeId(uint nSporeId)
    {
        m_uSporeId = nSporeId;
    }

    public uint GetSporeId()
    {
        return m_uSporeId;
    }

    public void SetPlayerId(int nPlayerId)
    {
        m_nPlayerId = nPlayerId;
    }

    public void SetColor( Color color )
    {
        _srMain.color = color;
    }

    public string GetKey()
    {
        return GetPlayerId() + "_" + GetSporeId();
    }

    public int GetPlayerId()
    {
        return m_nPlayerId;
    }

    public void SetBall(Ball ball)
    {
        m_Ball = ball;
    }

    public Ball GetBall()
    {
        return m_Ball;
    }

    public void SetDest(Vector2 dest)
    {
        m_vDestPos = dest;

        m_vSpeed = m_vDestPos - GetPos();
        m_vSpeed.Normalize();
        m_vDir = m_vSpeed;
        m_vSpeed *= CSkillSystem.s_Instance.m_SpitSporeParam.fEjectSpeed;
    }

    public Vector3 GetDest()
    {
        return m_vDestPos;
    }

    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    bool m_bDead = false;
    public bool isDead()
    {
        return m_bDead;
    }

    public void SetDead(bool bDead)
    {
        m_bDead = bDead;
       // this.gameObject.SetActive(!m_bDead);
       m_goContainer.SetActive(!m_bDead);
        _Collider.enabled = !m_bDead;
    }

    public Vector3 GetDir()
    {
        return m_vDir;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        if ( !IsEjecting() )
        {
            return;
        }
        if (other.transform.gameObject.tag != "thorn")
        {
            return;
        }
        CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
        if (thorn == null)
        {
            return;
        }

        this.EndEject();

        if (m_Ball == null)
        {
            return;
        }
        if (!m_Ball._Player.IsMainPlayer())
        {
            return;
        }
        vecTempPos = thorn.GetPos() - this.GetPos();
        vecTempPos.Normalize();
        m_Ball._Player.PushThorn( this.GetRadius(), thorn.GetGuid(), thorn.GetPos(), vecTempPos, this.GetDir());
    }
}
